#!/usr/bin/python

import sys

#############################################################################
# CONFIG
#############################################################################
# Skip first `skipLines` lines
skipLines = 3
# Replace the value of RI
RIdefault = 0
# 
exeDefault = 'cmdock'

#############################################################################
# ARGUMENTS CHECK
#############################################################################
if len(sys.argv) != 3:
    print('Wrong number of arguments')
    print('Usage: prepare.py inputFile outputFile')
    sys.exit(1)

inputFile = sys.argv[1]
outputFile = sys.argv[2]

#############################################################################
# INPUT FILE READING AND PROCESSING
#############################################################################
# Read the input file and skip first `skipLines` lines
with open(inputFile, 'r', newline=None) as f:
    inputFileStripped = f.readlines()[3:]

foundRI = False 
foundPath = False
foundCurrentDir = False
foundExecutable = False 
foundLibrary = False 
foundParameterFile = False
foundReceptor = False 

for i, line in enumerate(inputFileStripped):
            
    # Find <RI> field and replace its value in the next line with `RIdefault`
    if line == '>  <RI>\n':
        inputFileStripped[i+1] = '0\n'
    
    # Replace path separators
    foundCurrentDir = line == '>  <Rbt.Current_Directory>\n'
    foundExecutable = line == '>  <Rbt.Executable>\n'
    foundLibrary = line == '>  <Rbt.Library>\n'
    foundParameterFile = line == '>  <Rbt.Parameter_File>\n'
    foundReceptor = line == '>  <Rbt.Receptor>\n'

    foundPath = foundCurrentDir or foundExecutable or foundLibrary or foundParameterFile or foundReceptor
    if foundPath:
        inputFileStripped[i+1] = inputFileStripped[i+1].replace('\\','/')

    # Remove the path from the executable (on Windows it is full path, on Linux only the filename, ugh)
    if foundExecutable:
        last_sep = inputFileStripped[i+1].rfind('/')
        inputFileStripped[i+1] = exeDefault + inputFileStripped[i+1][last_sep:]
    
#############################################################################
# WRITE OUTPUT FILE
#############################################################################
with open(outputFile, 'w', newline='\n') as f:
    for line in inputFileStripped:
        f.write(line)

sys.exit(0)