test_env = environment({
  'CMDOCK_ROOT': meson.project_source_root(),
  'CMDOCK_HOME': meson.current_build_dir() / 'data' / 'input'
})

# Get test_cases and generate data files in the builddir
subdir('data' / 'input')

# Python is required for a cross-platform regression testing
# If not found, cmdock will still be launched but no tests will be performed
python = import('python').find_installation('python3', required : false)

if python.found()
  script_files = files('prepare.py', 'compare.py')
  foreach script_file : script_files
    configure_file(
      input : script_file,
      output : '@PLAINNAME@',
      copy : true
    )
  endforeach
endif

docking_out = {}
foreach prm, data_files : test_cases
  prm_path = meson.current_build_dir() / 'data' / 'input' / prm
  sd_path = prm_path.replace('.prm', '_c.sd')

  cmdock_out = custom_target(
    output : '@BASENAME@_out',
    input : prm_path,
    command : [binaries['cmdock'],
      '-r', '@INPUT@', '-i', sd_path, '-o', '@OUTPUT@',
      '-p', 'dock.prm', '-n', '1', '-s', '48151623', '-x'
    ],
    depends : data_files['dynamic'],
    depend_files : sd_path,
    console : true,
    env : test_env
  )
  
  if python.found() 

    cmdock_out_stripped = custom_target(
      input : cmdock_out,
      output : '@BASENAME@.stripped',
      command : [python, meson.current_build_dir() / 'prepare.py', '@INPUT@', '@OUTPUT@'],
      depend_files : ['prepare.py']
    )

    
    expected_subdir = 'output'
    cmdock_out_expected = configure_file(
      input : meson.current_source_dir() / 'data' / expected_subdir / f'@prm@.out',
      output : '@PLAINNAME@_expected',
      configuration : {
        'BUILDDIR': meson.project_build_root().replace('\\','/'),
        'SOURCEDIR': meson.project_source_root().replace('\\','/'),
        'VERSION': meson.project_version()
      }
    )

    docking_out += {
      prm: {
        'actual': cmdock_out_stripped,
        'expected': cmdock_out_expected
      }
    }

    # Define tests
    # to enable parallelism add option is_parallel : true
    test(f'test-cmdock[@prm@]', python,
      args : [
        meson.current_build_dir() / 'compare.py',
        docking_out[prm]['expected'],
        docking_out[prm]['actual'].full_path()
      ],
      depends : docking_out[prm]['actual'],
      suite : 'cmdock'
    )
  endif  
endforeach

# TODO: enable unit tests
#  gtest_dep = dependency(
#    'gtest', fallback : ['gtest', 'gtest_dep'], required : false
#  )
#  if gtest_dep.found()
#    incTest = include_directories('.')
#    srcTest = files(
#      'Main.cxx',
#      'OccupancyTest.cxx',
#      'RbtChromTest.cxx',
#      'SearchTest.cxx'
#    )
#    unit_test = executable(
#      'unit-test', srcTest,
#      dependencies : [pcg_cpp_dep, gtest_dep, eigen3_dep],
#      link_with : libcmdock, include_directories : [incTest, incRbt]
#    )
#    test(
#      'unit-test', unit_test,
#      env : test_env,
#      is_parallel : false,
#      timeout : 900
#    )
#  endif
