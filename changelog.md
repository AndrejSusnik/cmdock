## changelog

from 0.1.4 >
4_12_2022

### 0.2.0
 - Cross-platform consistent random number generator (custom implementation of probability density functions); new switch -x defined, which turns on this feature.
 - CI pipeline with regression testing
 - logging in cmcavity
 - cmcavity implementaiton of cavity volumes in InsightII (-I) or MRC 2014 (-M)
 - Full protein surface mapping
 - Pymol GUI plugin
