/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * This version is licensed under GNU-LGPL version 3.0 with support from
 * the University of Barcelona.
 * http://rdock.sourceforge.net/
 ***********************************************************************/

#include "RbtBaseFileSource.h"
#include "RbtFileError.h"
#include <cstring>
#include <sys/stat.h>
#include <streambuf>
#include <indicators/cursor_control.hpp>

template<typename CharT, typename TraitsT = std::char_traits<CharT> >
class vectorwrapbuf : public std::basic_streambuf<CharT, TraitsT> {
public:
    vectorwrapbuf(std::vector<CharT> &vec) {
        std::streambuf::setg(vec.data(), vec.data(), vec.data() + vec.size());
    }
};

// Constructors
// RbtBaseFileSource::RbtBaseFileSource(const char* fileName)
//{
//  m_strFileName = fileName;
//  ClearCache();
//  _RBTOBJECTCOUNTER_CONSTR_("RbtBaseFileSource");
//}

RbtBaseFileSource::RbtBaseFileSource(const std::string &fileName)
    : m_numReads(0), m_bytesRead(0), m_bFileOpen(false), m_bMultiRec(false),
      m_bZipped(false) {
  m_strFileName = fileName;
  struct stat fileStat;
  if (stat(fileName.c_str(), &fileStat) == 0) {
    m_fileSize = fileStat.st_size;
  }
  m_szBuf = new char[MAXLINELENGTH + 1]; // DM 24 Mar - allocate line buffer
  ClearCache();
  _RBTOBJECTCOUNTER_CONSTR_("RbtBaseFileSource");
}

// Multi-record constructor
RbtBaseFileSource::RbtBaseFileSource(const std::string &fileName,
                                     const std::string &strRecDelim)
    : m_numReads(0), m_bytesRead(0), m_bFileOpen(false), m_bMultiRec(true),
      m_strRecDelim(strRecDelim), m_bZipped(false) {
  m_strFileName = fileName;
  struct stat fileStat;
  if (stat(fileName.c_str(), &fileStat) == 0) {
    m_fileSize = fileStat.st_size;
  }
  m_szBuf = new char[MAXLINELENGTH + 1]; // DM 24 Mar - allocate line buffer
  ClearCache();
  _RBTOBJECTCOUNTER_CONSTR_("RbtBaseFileSource");
}

// Default destructor
RbtBaseFileSource::~RbtBaseFileSource() {
  Close();
  ClearCache();
  delete[] m_szBuf; // DM 24 Mar - delete line buffer
  _RBTOBJECTCOUNTER_DESTR_("RbtBaseFileSource");
}

// Public methods
std::string RbtBaseFileSource::GetFileName() { return m_strFileName; }

void RbtBaseFileSource::SetZipped(bool zipped) { 
  m_bZipped = zipped;
}

// void RbtBaseFileSource::SetFileName(const char* fileName)
//{
//  Close();
//  ClearCache();
//  m_strFileName = fileName;
//}

void RbtBaseFileSource::SetFileName(const std::string &fileName) {
  Close();
  ClearCache();
  m_strFileName = fileName;
}

// Status and StatusOK parse the file to check for errors
bool RbtBaseFileSource::StatusOK() { return Status().isOK(); }

RbtError RbtBaseFileSource::Status() {
  // Try parsing the file and see what we catch
  try {
    Parse();
    // If we get here then everything is fine
    return RbtError();
  }

  // Got an RbtError
  catch (RbtError &error) {
    return error;
  }
}

// FileStatus and FileStatusOK just try and read the file
bool RbtBaseFileSource::FileStatusOK() { return FileStatus().isOK(); }

RbtError RbtBaseFileSource::FileStatus() {
  // Try reading the file and see what we catch
  try {
    Read();
    // If we get here then everything is fine
    return RbtError();
  }

  // Got an RbtError
  catch (RbtError &error) {
    return error;
  }
}

// Multi-record methods

// Force the reading of the next record by clearing the cache
// Doesn't actually read the record
void RbtBaseFileSource::NextRecord() {
  if (m_bMultiRec) {
    ClearCache();
  }
}

// Rewind the file back to the first record
void RbtBaseFileSource::Rewind() {
  if (m_bMultiRec) {
    Close();
    ClearCache();
  }
}

// Estimate the number of records in the file from the file size
std::size_t RbtBaseFileSource::GetEstimatedNumRecords() {
  if (m_bZipped) {
    return m_archive.index.size() - 1;
  }
  if (m_bMultiRec) {
    if (m_numReads > 0 && m_bytesRead > 0) {
      double avgBytesRead =
          static_cast<double>(m_bytesRead) / static_cast<double>(m_numReads);
      double estNumRecords = static_cast<double>(m_fileSize) / avgBytesRead;
      return static_cast<std::size_t>(estNumRecords);
    } else {
      return 0;
    }
  }
  return 1;
}

// Protected functions

void RbtBaseFileSource::Read(bool aDelimiterAtEnd) {
  // If we haven't already read the file, do it now
  std::size_t bytesLastRead = 0;
  if (!m_bReadOK) {
    if (m_bZipped) {
      try {
        Open();
        ClearCache();
        m_archive.readNextRecord();
        m_archive.decompress();
        vectorwrapbuf<char> databuf(m_archive.decompressedRecord);
        std::istream stream(&databuf);
        const char *cszRecDelim = m_strRecDelim.c_str();
        int n = strlen(cszRecDelim);
        while ((stream.getline(m_szBuf, MAXLINELENGTH)) &&
              (strncmp(m_szBuf, cszRecDelim, n) != 0)) {
#ifdef _DEBUG
          std::cout << m_szBuf << std::endl;
#endif //_DEBUG
          bytesLastRead += std::strlen(m_szBuf) + 1;
          // adding 1 byte per getline for newline (LF) character, no need
          // to check for CRLF as they are considered unsupported
          m_lineRecs.push_back(m_szBuf);
        }
        if (m_lineRecs.empty())
          throw RbtFileReadError(_WHERE_, "End of file/empty record in " +
                                 m_strFileName);
      } catch (RbtError &error) {
        Close();
        throw;
      }
    } else {
      if (aDelimiterAtEnd) {
        ClearCache();
        try {
          Open();
          // Multi-record read
          // Only read up to record delimiter (or end of file)
          // and leave file open for next record
          if (m_bMultiRec) {
            const char *cszRecDelim = m_strRecDelim.c_str();
            int n = strlen(cszRecDelim);
            while ((m_fileIn.getline(m_szBuf, MAXLINELENGTH)) &&
                  (strncmp(m_szBuf, cszRecDelim, n) != 0)) {
#ifdef _DEBUG
              std::cout << m_szBuf << std::endl;
#endif //_DEBUG
              bytesLastRead += std::strlen(m_szBuf) + 1;
              // adding 1 byte per getline for newline (LF) character, no need
              // to check for CRLF as they are considered unsupported
              m_lineRecs.push_back(m_szBuf);
            }
          }
          // Single-record read
          // Read entire file and close immediately
          else {
            while (m_fileIn.getline(m_szBuf, MAXLINELENGTH)) {
              bytesLastRead += std::strlen(m_szBuf) + 1;
              m_lineRecs.push_back(m_szBuf);
            }
            Close();
          }
          // DM 25 Mar 1999 - check for end of file (i.e. no lines read)
          if (m_lineRecs.empty())
            throw RbtFileReadError(_WHERE_, "End of file/empty record in " +
                                                m_strFileName);
        }
        // Catch exceptions so we can close the file, then rethrow it
        catch (RbtError &error) {
          Close();
          throw;
        }
      } else { // ie MOL2 file records starts (not ends) with a certain pattern
        ClearCache();
        try {
          Open();
          // Multi-record read
          // Only read up to next record delimiter (or end of file)
          // and leave file open for next record
          if (m_bMultiRec) {
            const char *cszRecDelim = m_strRecDelim.c_str();
            int n = strlen(cszRecDelim);
            // skip to the header stuff until the first record
            // AND the first delimiter line
            while ((m_fileIn.getline(m_szBuf, MAXLINELENGTH)) &&
                  (strncmp(m_szBuf, cszRecDelim, n) != 0))
              ;
            while ((m_fileIn.getline(m_szBuf, MAXLINELENGTH)) &&
                  (strncmp(m_szBuf, cszRecDelim, n) != 0)) {
#ifdef _DEBUG
              std::cout << m_szBuf << std::endl;
#endif //_DEBUG
              bytesLastRead += std::strlen(m_szBuf) + 1;
              m_lineRecs.push_back(m_szBuf);
            }
          }
          // Single-record read
          // Read entire file and close immediately
          else {
            while (m_fileIn.getline(m_szBuf, MAXLINELENGTH)) {
              bytesLastRead += std::strlen(m_szBuf) + 1;
              m_lineRecs.push_back(m_szBuf);
            }
            Close();
          }
          // DM 25 Mar 1999 - check for end of file (i.e. no lines read)
          if (m_lineRecs.empty())
            throw RbtFileReadError(_WHERE_, "End of file/empty record in " +
                                                m_strFileName);
        }
        // Catch exceptions so we can close the file, then rethrow it
        catch (RbtError &error) {
          Close();
          throw;
        }
      }
    }
    // If we get to here, we read the file OK
    m_bReadOK = true;
    m_numReads++;
    m_bytesRead += bytesLastRead;
  }
}

// Private functions
void RbtBaseFileSource::Open() {
  // DM 23 Mar 1999 - check if file is already open, to allow Open() to be
  // called redundantly
  bool bArchiveOpened = false;
  if (!m_bFileOpen) {
    if (m_bZipped) {
      m_archive.inputFile.open(m_strFileName, std::ios_base::in);
      if (m_archive.readCompressedIndex()) {
        int iIndexAlgorithm = CmZ::findAlgorithm(m_archive.compressedIndex);
        if (!CmZ::isAlgorithmSupported(iIndexAlgorithm)){
            std::cerr << "Current build does not support " << CmZ::encoderName(iIndexAlgorithm) << std::endl;
            indicators::show_console_cursor(true);
            exit(1);
        }
        if (m_archive.decompressIndex()) {
          bArchiveOpened = true;
        } else {
          std::cerr << "Error decompressing index!" << std::endl;
        }
      } else {
        std::cerr << "Error reading compressed index!" << std::endl;
      }
    } else {
      m_fileIn.open(m_strFileName.c_str(), std::ios_base::in);
    }
  }

  // If file did not open, throw an error
  if (!(m_fileIn || bArchiveOpened))
    throw RbtFileReadError(_WHERE_, "Error opening " + m_strFileName);
  else
    m_bFileOpen = true;
}

void RbtBaseFileSource::Close() {
  if (m_bZipped) {
    m_archive.inputFile.close();
  } else {
    m_fileIn.close();
  }
  m_bFileOpen = false;
}

void RbtBaseFileSource::ClearCache() {
  m_lineRecs.clear();  // Get rid of the previous file records
  m_bReadOK = false;   // Indicate the cache is invalid
  m_bParsedOK = false; // Tell the Parse function in derived classes that
  // it will have to reparse the file
}
