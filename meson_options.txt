# See INSTALL.md for build instructions.

option(
  'tests',
  type : 'boolean',
  value : false,
  description : 'Build the regression tests'
)

option(
  'docdir',
  type : 'string',
  value : '',
  description : 'Path or doc/ subdirectory name for installing HTML documentation'
)

option(
  'doc',
  type : 'boolean',
  value : false,
  description : 'Build the documentation with Sphinx'
)

option(
  'apidoc',
  type : 'boolean',
  value : false,
  description : 'Build the API documentation with Doxygen'
)

option(
  'buildsysinstalldir',
  type : 'boolean',
  value : false,
  description : 'Running meson install will build directory structure compatible with unix system installations.'
)

option('cmdock',     type : 'feature', value : 'auto')
option('cmcavity',   type : 'feature', value : 'auto')
option('cmcalcgrid',   type : 'feature', value : 'auto')
option('cmrms',      type : 'feature', value : 'auto')
option('cmmoegrid',  type : 'feature', value : 'auto')
option('cmlist',     type : 'feature', value : 'auto')
option('cmconvgrid', type : 'feature', value : 'auto')
option('cmzip',      type : 'feature', value : 'auto')

option('cmtether',   type : 'feature', value : 'auto')
option('smart_rms',  type : 'feature', value : 'auto')