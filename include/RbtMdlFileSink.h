/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * This version is licensed under GNU-LGPL version 3.0 with support from
 * the University of Barcelona.
 * http://rdock.sourceforge.net/
 ***********************************************************************/

// Class for writing RbtModel's to MDL SD and MOL files

#ifndef _RBTMDLFILESINK_H_
#define _RBTMDLFILESINK_H_

#include "RbtBaseMolecularFileSink.h"
#include "RbtElementFileSource.h"
#include "RbtCompression.hpp"
#include "RbtHeap.hpp"

// DM 19 June 2006
// Map to keep track of logical atom IDs as they are rendered to file
// This avoids the need for the atoms in the RbtModel to have consecutive
// atom IDs. The rendering of enabled solvent models is now supported
// correctly.
// Key = RbtAtom* pointer
// Value = logical atom ID as rendered to file
// Logical atom IDs are consecutive for all atoms rendered
// The actual atom ID (RbtAtom::GetAtomId()) is unchanged
typedef std::map<RbtAtom *, unsigned int, Rbt::RbtAtomPtrCmp_Ptr> RbtAtomIdMap;

class RbtMdlFileSink : public RbtBaseMolecularFileSink {
public:
  ////////////////////////////////////////
  // Constructors/destructors
  RBTDLL_EXPORT RbtMdlFileSink(const std::string &fileName,
                               RbtModelPtr spModel,
                               bool checkpointMode = false);

  virtual ~RbtMdlFileSink(); // Default destructor

  ////////////////////////////////////////
  // Public methods
  ////////////////
  void SetZip(bool bZip, int iAlgorithm) {
    m_archive.mode = iAlgorithm;
    m_bZip = bZip;
  }
  void SetBestPoses(unsigned long uBestPoses) {
    m_bBestPoses = true;
    m_ligandHeap.setSize(uBestPoses);
  }
  void SetBestPoses(unsigned long uBestPoses, std::string strBestPoseCriterium) {
    m_bBestPoses = true;
    m_ligandHeap.setSize(uBestPoses);
    m_strBestPoseCriterium = strBestPoseCriterium;
  }
  void SetBestPoses(std::string strBestPoseCLArgument) {
    unsigned long uBestPoses;
    std::string strBestPoseCriterium;
    try {
      if (strBestPoseCLArgument.rfind(",", 0) == std::string::npos) {
        uBestPoses = std::stoul(strBestPoseCLArgument);
        SetBestPoses(uBestPoses);
      } else {
        int pos = strBestPoseCLArgument.find(",");
        uBestPoses = std::stoul(strBestPoseCLArgument.substr(0, pos));
        strBestPoseCriterium = strBestPoseCLArgument.substr(pos + 1, strBestPoseCLArgument.length());
        SetBestPoses(uBestPoses, strBestPoseCriterium);
      }
    } catch (...) {
      std::cerr << "Specified best poses argument is invalid!";
      exit(1);
    }
  }
  void SetBestPoseCriterium(std::string strBestPoseCriterium) {
    m_strBestPoseCriterium = strBestPoseCriterium;
  }
  void WriteBestPoses(bool bClearCache = true);
  //
  //
  ////////////////////////////////////////
  // Override public methods from RbtBaseFileSink
  virtual void Render();

protected:
  ////////////////////////////////////////
  // Protected methods
  ///////////////////
  std::string GetFieldData(const RbtStringVariantMap &dataMap, std::string fieldName);
  void Write(bool bClearCache = true);

private:
  ////////////////////////////////////////
  // Private methods
  /////////////////
  void RenderAtomList(const RbtAtomList &atomList);
  void RenderBondList(const RbtBondList &bondList);
  void RenderData(const RbtStringVariantMap &dataMap);

  RbtMdlFileSink();                       // Disable default constructor
  RbtMdlFileSink(const RbtMdlFileSink &); // Copy constructor disabled by
                                          // default
  RbtMdlFileSink &
  operator=(const RbtMdlFileSink &); // Copy assignment disabled by default

  void Open(bool bAppend);
  void Close();

protected:
  ////////////////////////////////////////
  // Protected data
  ////////////////

private:
  ////////////////////////////////////////
  // Private data
  //////////////
  RbtElementFileSourcePtr m_spElementData;
  // DM 27 Apr 1999 - default behaviour is for the first Render to overwrite any
  // existing file then subsequent Renders to append.
  bool m_bFirstRender;
  bool m_bFirstOpen = true;
  RbtAtomIdMap m_atomIdMap; // Keep track of logical atom IDs as rendered to
                            // file
  bool m_bAppend; // If true, Write() appends to file rather than overwriting
  bool m_bCheckpointMode = false;
  bool m_bZip = false;
  bool m_bBestPoses = false;
  std::string m_strBestPoseCriterium = "SCORE.INTER";
  CmZ::archive m_archive;
  RbtHeap::Heap m_ligandHeap;
  std::vector<std::string> m_convertToKCAL = {
    "SCORE", "SCORE.INTER", "SCORE.INTRA"
  };
};

// Useful typedefs
typedef SmartPtr<RbtMdlFileSink> RbtMdlFileSinkPtr; // Smart pointer

namespace Rbt {
void RenumberScaffold(RbtAtomList &atomList);
}

#endif //_RBTMDLFILESINK_H_
